package server

fun getDocumento(claveAcceso : String) : String {
    return claveAcceso.substring(10, 23)
}

fun getTipoComprobante(claveAcceso: String) : TipoComprobante {
    val tipoComprobante = claveAcceso.substring(8, 10)
    println("Código tipo comprobante: $tipoComprobante")
    when (tipoComprobante) {
        "01" -> return TipoComprobante.FACTURA
        "04" -> return TipoComprobante.NOTA_CREDITO
        "05" -> return TipoComprobante.NOTA_DEBITO
        "06" -> return TipoComprobante.GUIA
        "07" -> return TipoComprobante.RETENCION
    }
    return TipoComprobante.NO_DEFINIDO
}