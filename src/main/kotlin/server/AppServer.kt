package server

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
//import io.ktor.server.engine.*
//import io.ktor.server.netty.*

fun Application.main(/*args: Array<String>*/) {
    initDB()
//    val server = embeddedServer(Netty, port = 8080) {
        routing {
            route("/printer") {
                get("/welcome") {
                    call.respondText("Aplicación para generar el RIDE a partir de un XML", ContentType.Text.Plain)
                    printParametros()
                }

                getXml()

                getPdf()
            }
        }
//    }
//    server.start(wait = true)
}
